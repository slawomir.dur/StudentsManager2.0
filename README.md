This is a programme to manage a database of students.
It allows adding, deleting and browsing students.
Also, it enables the user to add lessons to each students, send periodical
summaries and send notifications with custom text message via a smtp client.

Dependencies:
Qt5
SmtpClient-for-Qt by bluetiger9

