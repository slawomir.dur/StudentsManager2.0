TEMPLATE=app
TARGET=TestProject

QT=core gui

greaterThan(QT_MAJOR_VERSION, 4):QT+=widgets

QT+=sql
QT+=network

SOURCES += \
    testproject2.cpp \
    mainwindow.cpp \
    menubar.cpp \
    actions.cpp \
    newlesson.cpp \
    students.cpp \
    notification.cpp \
    lessons.cpp \
    summary.cpp \
    database.cpp \
    studentstable.cpp \
    lessonstable.cpp \
    comboboxmodel.cpp \
    nameconverter.cpp \
    mailmanager.cpp \
    smtpsettings.cpp

HEADERS += \
    mainwindow.h \
    menubar.h \
    actions.h \
    newlesson.h \
    students.h \
    notification.h \
    lessons.h \
    summary.h \
    database.h \
    studentstable.h \
    lessonstable.h \
    comboboxmodel.h \
    nameconverter.h \
    mailmanager.h \
    smtpsettings.h

win32:CONFIG(release, debug|release): LIBS += -L$$PWD/../../../../Libraries/build-SMTPEmail-Desktop-Debug/release/ -lSMTPEmail
else:win32:CONFIG(debug, debug|release): LIBS += -L$$PWD/../../../../Libraries/build-SMTPEmail-Desktop-Debug/debug/ -lSMTPEmail
else:unix: LIBS += -L$$PWD/../../../../Libraries/build-SMTPEmail-Desktop-Debug/ -lSMTPEmail

INCLUDEPATH += $$PWD/../../../../Libraries/build-SMTPEmail-Desktop-Debug
DEPENDPATH += $$PWD/../../../../Libraries/build-SMTPEmail-Desktop-Debug
