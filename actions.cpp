#include "actions.h"
#include "database.h"
#include <QDebug>
#include <iostream>

Actions::Actions(QWidget *parent) : QStackedWidget(parent)
{
    newLesson = new NewLesson(this);
    students = new Students(this);
    notification = new Notification(this);
    lessons = new Lessons(this);
    summary = new Summary(this);
    smtpsettings = new SMTPSettings(this);

    addWidget(newLesson);
    addWidget(students);
    addWidget(notification);
    addWidget(lessons);
    addWidget(summary);
    addWidget(smtpsettings);
    makeConnections();
}

void Actions::makeConnections()
{
    //qDebug() << "trying to connect to addwindow";
    connect(students->getAddWindow(), SIGNAL(dataChanged()), this, SLOT(repaintTable()));
    connect(students->getRemoveWindow(), SIGNAL(dataChanged()), this, SLOT(repaintTable()));
    connect(students->getAddWindow(), SIGNAL(dataChanged()), students->getRemoveWindow()->getComboBox(), SLOT(redrawComboBox()));
    connect(students->getAddWindow(), SIGNAL(dataChanged()), lessons->getComboBox(), SLOT(redrawComboBox()));
    connect(students->getAddWindow(), SIGNAL(dataChanged()), newLesson->getComboBox(), SLOT(redrawComboBox()));
    connect(students->getAddWindow(), SIGNAL(dataChanged()), notification->getComboBox(), SLOT(redrawComboBox()));
    connect(students->getAddWindow(), SIGNAL(dataChanged()), summary->getComboBox(), SLOT(redrawComboBox()));
    connect(students->getRemoveWindow(), SIGNAL(dataChanged()), students->getRemoveWindow()->getComboBox(), SLOT(redrawComboBox()));
    connect(students->getRemoveWindow(), SIGNAL(dataChanged()), lessons->getComboBox(), SLOT(redrawComboBox()));
    connect(students->getRemoveWindow(), SIGNAL(dataChanged()), newLesson->getComboBox(), SLOT(redrawComboBox()));
    connect(students->getRemoveWindow(), SIGNAL(dataChanged()), notification->getComboBox(), SLOT(redrawComboBox()));
    connect(students->getRemoveWindow(), SIGNAL(dataChanged()), summary->getComboBox(), SLOT(redrawComboBox()));
    connect(lessons->getComboBox()->getComboBox(), SIGNAL(activated(QString)), lessons->getLessonsTable(), SLOT(setTable(QString)));
    connect(newLesson, SIGNAL(dataChanged()), lessons->getLessonsTable(), SLOT(repaintTable()));
}

void Actions::repaintTable()
{
    //qDebug() << "repaintTable() from actions.cpp";
    Database db;
    db.connect();
    students->getStudsTable()->getTableModel()->select();
    students->getStudsTable()->getTable()->repaint();
}
