#ifndef ACTIONS_H
#define ACTIONS_H

#include <QStackedWidget>
#include "newlesson.h"
#include "students.h"
#include "notification.h"
#include "lessons.h"
#include "summary.h"
#include "smtpsettings.h"

class Actions : public QStackedWidget
{
    Q_OBJECT
public:
    enum ACTION {NEWLESSON, STUDENTS, NOTIFICATION, LESSONS, SUMMARY, SMTPSETTINGS};
public:
    Actions(QWidget *parent = nullptr);
    AddWindow* getAddWindow() {return students->getAddWindow();}
    RemoveWindow* getRemoveWindow() {return students->getRemoveWindow();}
    Lessons* getLessons() {return lessons;}
private:
    void makeConnections();
private:
    NewLesson *newLesson;
    Students *students;
    Notification *notification;
    Lessons *lessons;
    Summary *summary;
    SMTPSettings *smtpsettings;
signals:
public slots:
    void repaintTable();
};

#endif // ACTIONS_H
