#include <QVBoxLayout>
#include "comboboxmodel.h"
#include "database.h"
#include <QDebug>

ComboBoxModel::ComboBoxModel(QWidget* parent) : QWidget(parent)
{
   // qDebug() << "ComboBoxModel init";
   model = new QStringListModel;
   QStringList list;

   //qDebug() << "initialisign db";
   db = new Database;
   //qDebug() << "Before connect";
   db->connect();
   //qDebug() << "before getStudentList()";
   db->getStudentList(list);

   model->setStringList(list);

   QVBoxLayout *layout = new QVBoxLayout;

   comboBox = new QComboBox;

   comboBox->setModel(model);

   layout->addWidget(comboBox);
   setLayout(layout);
   //qDebug() << "ComboBoxModel init end";
}

void ComboBoxModel::redrawComboBox()
{
    QStringList list;
    db->connect(); // is it necessary?
    db->getStudentList(list);

    model->setStringList(list);
    comboBox->repaint();
}
