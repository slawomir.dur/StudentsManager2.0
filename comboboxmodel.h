#ifndef COMBOBOXMODEL_H
#define COMBOBOXMODEL_H

#include <QComboBox>
#include <QStringListModel>
#include "database.h"

class ComboBoxModel : public QWidget
{
    Q_OBJECT
public:
    ComboBoxModel(QWidget* parent = nullptr);
    QString currentText() {return comboBox->currentText();}
    QComboBox* getComboBox() {return comboBox;}
    QStringListModel* getModel() {return model;}
private:
    Database *db;
    QComboBox *comboBox;
    QStringListModel *model;
public slots:
    void redrawComboBox();
};

#endif // COMBOBOXMODEL_H
