#include "database.h"
#include "nameconverter.h"
#include <QSqlQuery>
#include <QSqlRecord>
#include <QDebug>
#include <QFile>
#include <QSqlError>

QSqlDatabase Database::db = QSqlDatabase::addDatabase("QSQLITE");

Database::Database() : mode(MODE::STUDENTS)
{
    db.setHostName(HOST);
    db.setDatabaseName(DATABASE_NAME);
    if (connect())
        createGeneralTable();
}

bool Database::connect()
{
    if (QFile(DATABASE_NAME).exists())
        return db.open();
    else return false;
}

bool Database::createGeneralTable()
{
    QSqlQuery query;
    if (!query.exec("CREATE TABLE IF NOT EXISTS STUDENTS("
                    "name VARCHAR(25) NOT NULL,"
                    "surname VARCHAR(25) NOT NULL,"
                    "phone VARCHAR(12) NOT NULL,"
                    "mail VARCHAR(40) NOT NULL)"))
        return false;
    else return true;
}

bool Database::createSpecificTable(const QVariant& tableName)
{
    //qDebug() << "createSpecificTable entered";
    QString query_text("CREATE TABLE IF NOT EXISTS " + tableName.toString() + "(subject VARCHAR(50) NOT NULL, "
                                                                              "date VARCHAR(15), "
                                                                              "homework VARCHAR(60), "
                                                                              "notes TEXT)");
    //qDebug() << "Querytext: " << query_text;
    QSqlQuery query;
    if (query.exec(query_text))
    {
        //qDebug() << "createSpecificTable successful";
        return true;
    }
    else
    {
        //qDebug() << query.lastError();
        //qDebug() << query.executedQuery();
        return false;
    }
}

bool Database::insert(const QVariantList& list, const Database::MODE& desiredMode, const QVariant *TableName)
{
    if (mode == MODE::STUDENTS && mode == desiredMode)
    {
        QSqlQuery query;
        QString name = list[0].toString();
        QString surname = list[1].toString();
        query.prepare("INSERT INTO STUDENTS(name, surname, phone, mail) VALUES (:Name, :Surname, :Phone, :Mail)");
        query.bindValue(":Name", name);
        qDebug() << name;
        query.bindValue(":Surname", surname);
        qDebug() << surname;
        query.bindValue(":Phone", list[2].toString());
        qDebug() << list[2].toString();
        query.bindValue(":Mail", list[3].toString());
        qDebug() << list[3].toString();
        if (query.exec())
        {
            qDebug() << query.lastQuery();
            //qDebug() << "Query.exec() from insert passes";
            QVariant studentTable(name + surname);
            if (createSpecificTable(studentTable))
            {
                //qDebug() << "Insert from database.cpp returns true";
                return true;
            }
            else
            {
                //qDebug() << query.lastError();
                return false;
            }
        }
        else return false;

    }
    else if (mode == MODE::LESSONS && mode == desiredMode)
    {
        //qDebug() << "db.insert() in LESSONS mode entered";
        /*      Though good it seems, this solution generates "Parameter mismatch error".
         *      Hence, I needed to implement a barbarian, though working, solution
         *      basing on QString and simple concatenation.
         *
        QSqlQuery query;
        query.prepare("INSERT INTO :TableName(subject, date, homework, notes) VALUES (:Subject, :Date, :Homework, :Notes)");
        query.bindValue(":TableName", (*TableName).toString());
        query.bindValue(":Subject", list[0].toString());
        query.bindValue(":Date", list[1].toString());
        query.bindValue(":Homework", list[2].toString());
        query.bindValue(":Notes", list[3].toString());
        */

        QString queryText("INSERT INTO " + (*TableName).toString() + "(subject, date, homework, notes)"
                                                                     " VALUES (\"" + list[0].toString() + "\", "
                                                                     "\"" + list[1].toString() + "\", "
                                                                     "\"" + list[2].toString() + "\", "
                                                                     "\"" + list[3].toString() + "\")");
        //qDebug() << queryText;
        QSqlQuery query;
        if (query.exec(queryText))
            return true;
        else
        {
            //qDebug() << "Error: " << query.lastError();
            return false;
        }
    }
    else return false;
}

bool Database::deleteRecord(const QVariantList& list, const Database::MODE& desiredMode, const QVariant *TableName)
{
    if (mode == MODE::STUDENTS && desiredMode == MODE::STUDENTS)
    {
        QSqlQuery query;
        query.prepare("DELETE FROM STUDENTS WHERE NAME = :Name AND SURNAME = :Surname");
        query.bindValue(":Name", list[0].toString());
        query.bindValue(":Surname", list[1].toString());

        if (query.exec())
        { // Here the corresponding database of a students should also be deleted!
            QString queryText("DROP TABLE " + list[0].toString()+list[1].toString());
            //qDebug() << queryText;
            if (query.exec(queryText))
                return true;
            else
            {
               // qDebug() << query.lastError();
                return false;
            }
        }
        else return false;
    }
    else if (mode == MODE::LESSONS && desiredMode == MODE::LESSONS)
    {
        //qDebug() << "deleteRecord() entered where it should";
        QSqlQuery query;
        /*query.prepare("DELETE FROM :Table WHERE SUBJECT = :Subject");
        query.bindValue(":Table", TableName->toString());
        query.bindValue(":Subject", list[0].toString());
        */
        QString queryText("DELETE FROM " + TableName->toString() + " WHERE SUBJECT = \"" + list[0].toString() + "\"");
        //qDebug() << "Query text: " << queryText;
        if (query.exec(queryText))
            return true;
        else
        {
            //qDebug() << query.lastError();
            return false;
        }
    }
    else return false;
}

bool Database::setMode(const Database::MODE& mode)
{
    if (mode == MODE::STUDENTS)
    {
        this->mode = mode;
        return true;
    }
    else if (mode == MODE::LESSONS)
    {
        this->mode = mode;
        return true;
    }
    else return false;
}

bool Database::open()
{
    if (db.isOpen())
        return true;
    else
    {
        if (db.open())
            return true;
        else return false;
    }
}

void Database::close()
{
    if (db.isOpen())
        db.close();
}

void Database::getStudentList(QStringList &list)
{
    int init_state = mode;
    mode = Database::STUDENTS;
    QSqlQuery query;
    query = QSqlQuery("SELECT * FROM STUDENTS");
    int nameIndex = query.record().indexOf("name");
    int surnameIndex = query.record().indexOf("surname");
    QString name;
    QString surname;
    while (query.next())
    {
        name = query.value(nameIndex).toString();
        surname = query.value(surnameIndex).toString();
        list << (name + " " + surname);
    }
    mode = init_state;
}

QString Database::getStudentMail(QString& nameWithSpace)
{
    qDebug() << "getStudentMail()";
    int init_state = mode;
    mode = Database::STUDENTS;
    auto namesurname = splitToSeparate(nameWithSpace.toStdString());
    QString name = namesurname.first;
    QString surname = namesurname.second;
    QString query_text("SELECT MAIL FROM STUDENTS WHERE NAME = \"" + name + "\" AND SURNAME = \"" + surname + "\"");
    qDebug() << "query execution";
    QSqlQuery query(query_text);
    QString mail;
    while (query.next())
    {
        mail = query.value(0).toString();
        qDebug() << "mail: " << mail;
    }
    mode = init_state;
    return mail;
}


