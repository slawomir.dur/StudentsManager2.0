#ifndef DATABASE_H
#define DATABASE_H

#include <QVariantList>
#include <QSqlDatabase>

#define HOST "HOST"
#define DATABASE_NAME "StudentsManager.db"

class Database
{
public:
    enum MODE {STUDENTS, LESSONS};
public:
    Database();
    ~Database() {close();}
    bool connect();
    bool createGeneralTable();
    bool createSpecificTable(const QVariant& tableName);
    bool insert(const QVariantList& list, const Database::MODE& desiredMode, const QVariant *TableName = nullptr);
    bool deleteRecord(const QVariantList& list, const Database::MODE& desiredMode, const QVariant *TableName = nullptr);
    bool setMode(const Database::MODE& mode);
    void getStudentList(QStringList &list);
    QString getStudentMail(QString& nameWithSpace);
private:
    bool open();
    void close();
private:
    int mode;
    static QSqlDatabase db;
};

#endif // DATABASE_H
