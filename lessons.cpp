#include "lessons.h"
#include <QVBoxLayout>
#include <QHBoxLayout>
#include <QLabel>
#include <QDebug>
#include <QMessageBox>
#include "database.h"
#include "nameconverter.h"

Lessons::Lessons(QWidget *parent) : QWidget(parent)
{
    deleteWindow = new DeleteWindow(this);
    QVBoxLayout *primeLayout = new QVBoxLayout;
    QHBoxLayout *choiceLayout = new QHBoxLayout;
    QHBoxLayout *buttonLayout = new QHBoxLayout;

    QLabel *choiceLabel = new QLabel(tr("Choose the student: "));
    lessonsTable = new LessonsTable;
    addButton = new QPushButton(tr("New Lesson"));
    deleteButton = new QPushButton(tr("Delete Lesson"));
    choiceBox = new ComboBoxModel;

    choiceLayout->addWidget(choiceLabel);
    choiceLayout->addWidget(choiceBox);

    buttonLayout->addWidget(addButton);
    buttonLayout->addWidget(deleteButton);

    primeLayout->addLayout(choiceLayout);
    primeLayout->addWidget(lessonsTable);
    primeLayout->addLayout(buttonLayout);

    setLayout(primeLayout);
    makeConnections();
}

void Lessons::makeConnections()
{
    connect(deleteButton, SIGNAL(clicked(bool)), this, SLOT(del_slot()));
    connect(deleteWindow, SIGNAL(dataChanged()), lessonsTable, SLOT(repaintTable()));
}

void Lessons::del_slot()
{
    //qDebug() << "del_slot()";
    deleteWindow->setModal(true);
    deleteWindow->exec();
}

DeleteWindow::DeleteWindow(QWidget *parent) : QDialog(parent)
{
    QVBoxLayout *primeLayout = new QVBoxLayout;
    QHBoxLayout *subjectLayout = new QHBoxLayout;
    QHBoxLayout *buttons = new QHBoxLayout;

    QLabel *subjectLabel = new QLabel(tr("Subject:"));
    subjectEdit = new QLineEdit;

    delButton = new QPushButton(tr("Delete"));

    cancelButton = new QPushButton(tr("Cancel"));
    subjectLayout->addWidget(subjectLabel);
    subjectLayout->addWidget(subjectEdit);

    buttons->addWidget(delButton);
    buttons->addWidget(cancelButton);

    primeLayout->addLayout(subjectLayout);
    primeLayout->addLayout(buttons);

    setLayout(primeLayout);

    makeConnections();
}

void DeleteWindow::makeConnections()
{
    connect(delButton, SIGNAL(clicked(bool)), this, SLOT(del_slot()));
    connect(cancelButton, SIGNAL(clicked(bool)), this, SLOT(cancel_slot()));
}

void DeleteWindow::del_slot()
{
    if (subjectEdit->text() != QString(""))
    {
        if (auto less = dynamic_cast<Lessons*>(parentWidget()))
        {
            QVariantList list;
            list.append(subjectEdit->text());
            Database db;
            db.connect();
            db.setMode(Database::LESSONS);
            QVariant tableName(convert(less->getComboBox()->currentText()));
            //qDebug() << "Parameter passed: " << tableName.toString();
            if (db.deleteRecord(list, Database::LESSONS, &tableName))
            {
                subjectEdit->setText("");
                emit dataChanged();
                QMessageBox msg(this);
                msg.setText(tr("Lesson deleted"));
                msg.setStandardButtons(QMessageBox::Ok);
                msg.exec();
            }
            else
            {
                QMessageBox msg(this);
                msg.setText(tr("Database error! Report to service!"));
                msg.setStandardButtons(QMessageBox::Ok);
                msg.exec();
            }
        }
        else
        {
            QMessageBox msg(this);
            msg.setText(tr("Cast problem. Report to service!"));
            msg.setStandardButtons(QMessageBox::Ok);
            msg.exec();
        }
    }
    else
    {
        QMessageBox msg(this);
        msg.setText(tr("Subject field empty."));
        msg.setStandardButtons(QMessageBox::Ok);
        msg.exec();
    }



}

void DeleteWindow::cancel_slot()
{
    close();
}
