#ifndef LESSONS_H
#define LESSONS_H

#include <QWidget>
#include <QPushButton>
#include <QDialog>
#include <QLineEdit>
#include "lessonstable.h"
#include "comboboxmodel.h"

class DeleteWindow;

class Lessons : public QWidget
{
    Q_OBJECT
public:
    explicit Lessons(QWidget *parent = nullptr);
    ComboBoxModel* getComboBox() {return choiceBox;}
    LessonsTable* getLessonsTable() {return lessonsTable;}
    QPushButton* getAddButton() {return addButton;}
    QPushButton* getDeleteButton() {return deleteButton;}
    DeleteWindow* getDeleteWindow() {return deleteWindow;}
private:
    void makeConnections();
private:
    LessonsTable *lessonsTable;
    ComboBoxModel *choiceBox;
    QPushButton *addButton;
    QPushButton *deleteButton;
    DeleteWindow* deleteWindow;
signals:
public slots:
    void del_slot();
};

class DeleteWindow : public QDialog
{
    Q_OBJECT
public:
    explicit DeleteWindow(QWidget *parent = nullptr);
private:
    void makeConnections();
private:
    QLineEdit* subjectEdit;
    QPushButton* delButton;
    QPushButton* cancelButton;
signals:
    void dataChanged();
public slots:
    void del_slot();
    void cancel_slot();
};

#endif // LESSONS_H
