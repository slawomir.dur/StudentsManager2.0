#include "lessonstable.h"
#include <QHeaderView>
#include <QHBoxLayout>
#include <QDebug>
#include <sstream>

LessonsTable::LessonsTable(QWidget *parent) : QWidget(parent)
{
    //qDebug() << "LessonsTable init";
    db = new Database;
    db->connect();

    headers.push_back(tr("Subject"));
    headers.push_back(tr("Date"));
    headers.push_back(tr("Homework"));
    headers.push_back(tr("Notes"));

    tableModel = new QSqlTableModel(this);
    tableModel->setTable(""); // this name is wrong; this is to set through a combobox

    for (int i = 0; i < tableModel->columnCount(); i++)
    {
       tableModel->setHeaderData(i, Qt::Horizontal, headers[i]);
    }
    /*for (auto i : headers)
    {
        static auto counter = 0;
        tableModel->setHeaderData(counter, Qt::Horizontal, i);
        ++counter;
    }
*/

    tableView = new QTableView;
    tableView->setModel(tableModel);
    tableView->setSelectionBehavior(QAbstractItemView::SelectRows);
    tableView->setSelectionMode(QAbstractItemView::SingleSelection);
    tableView->resizeColumnsToContents();
    tableView->setEditTriggers(QAbstractItemView::NoEditTriggers);
    tableView->horizontalHeader()->setSectionResizeMode(QHeaderView::Stretch);

    tableModel->select();

    QHBoxLayout *layout = new QHBoxLayout;
    layout->addWidget(tableView);
    setLayout(layout);
    //qDebug() << "LessonsTable init end";
}

void LessonsTable::setTable(const QString& TableName)
{
    //qDebug() << TableName;
    db->connect();
    std::string tabName;
    tabName = TableName.toStdString();
    std::stringstream stream;
    stream << tabName;
    std::string name, surname;
    stream >> name >> surname;
    QString Qname(name.c_str());
    QString Qsurname(surname.c_str());

    //qDebug() << QString(Qname+Qsurname);

    tableModel->setTable(QString(Qname + Qsurname));
    for (int i = 0; i < tableModel->columnCount(); i++)
    {
       tableModel->setHeaderData(i, Qt::Horizontal, headers[i]);
    }
    tableModel->select();
    tableView->repaint();
}

void LessonsTable::repaintTable()
{
    db->connect();
    tableModel->select();
    tableView->repaint();
}
