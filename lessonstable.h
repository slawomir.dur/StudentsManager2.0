#ifndef LESSONSTABLE_H
#define LESSONSTABLE_H

#include <QWidget>
#include <QSqlTableModel>
#include <QTableView>
#include <vector>
#include "database.h"

class LessonsTable : public QWidget
{
    Q_OBJECT
public:
    explicit LessonsTable(QWidget *parent = nullptr);
private:
    QSqlTableModel *tableModel;
    QTableView *tableView;
    Database *db;
    std::vector<QString> headers;
signals:

public slots:
    void setTable(const QString& TableName);
    void repaintTable();
};

#endif // LESSONSTABLE_H
