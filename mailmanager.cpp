#include "mailmanager.h"
#include <QDebug>
#include <QMessageBox>

QString MailManager::username = "";
QString MailManager::userlabel = "Private English Teacher";
QString MailManager::password = "";
int MailManager::port = 0;

MailManager::MailManager(const QString& host, const int& port) : smtp(host, port, SmtpClient::SslConnection)
{
   if (port == 0)
       smtp.setPort(465);
}

void MailManager::setUser(const QString& user)
{
    qDebug() << "setUser()";
    username = user;
}

void MailManager::setUserLabel(const QString &userlabel)
{
    if (userlabel != "")
        MailManager::userlabel = userlabel;
}

void MailManager::setPassword(const QString& password)
{
    qDebug() << "setPassword()";
    this->password = password;
}

void MailManager::setSendingInfo(const QString& toAddress, const QString& toLabel,
                                 const QString& subject, const QString* text)
{
    if (text != nullptr)
    {
        qDebug() << "setSendingInfo() with text";
        message.setSender(new EmailAddress(username, userlabel));
        message.addRecipient(new EmailAddress(toAddress, toLabel));
        message.setSubject(subject);
        message_text.setText(*text);
        message.addPart(&message_text);
    }
    else
    {
        qDebug() << "setSendingInfo() without text";
        message.setSender(new EmailAddress(username, userlabel));
        message.addRecipient(new EmailAddress(toAddress, toLabel));
        message.setSubject(subject);
    }

}

void MailManager::setText(const QString& body)
{
    qDebug() << "setText()";
    message_text.setText(body);
    message.addPart(&message_text);
}

bool MailManager::send()
{
    if (username != nullptr && password != nullptr)
    {
        qDebug() << "send()";
        smtp.setUser(username);
        qDebug() << "user set";
        smtp.setPassword(password);
        qDebug() << "password set";
        if (smtp.connectToHost())
        {
            if (smtp.login())
            {
                auto ret = smtp.sendMail(message);
                smtp.quit();
                return ret;
            }
            else return false;
        }
        else return false;
    }
    else qDebug() << "username or password not set";
}
