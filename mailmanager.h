#ifndef MAILMANAGER_H
#define MAILMANAGER_H

#include "../SmtpClient-for-Qt/src/SmtpMime"
#include <memory>
/*
typedef std::unique_ptr<QString> QStringPtr;
typedef std::unique_ptr<SmtpClient> SmtpPtr;
typedef std::unique_ptr<MimeMessage> MimeMessagePtr;
typedef std::unique_ptr<MimeText> MimeTextPtr;
typedef std::unique_ptr<EmailAddress> EmailAddressPtr;
*/
class MailManager
{
public:
    MailManager(const QString& host = "smtp.gmail.com", const int& port = port);
    static QString& User() {return username;}
    static QString& UserLabel() {return userlabel;}
    static QString& Password() {return password;}
    static int& Port() {return port;}
    void setUser(const QString& user);
    static void setUserLabel(const QString& userlabel);
    void setPassword(const QString& password);
    void setSendingInfo(const QString& toAddress, const QString& toLabel,
                                     const QString& subject, const QString* text = nullptr);
    void setText(const QString& body);
    bool send();
private:
    SmtpClient smtp;
    MimeMessage message;
    MimeText message_text;
    static QString username;
    static QString userlabel;
    static QString password;
    static int port;
};

#endif // MAILMANAGER_H
