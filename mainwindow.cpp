#include "mainwindow.h"
#include <QVBoxLayout>
#include <QMessageBox>
#include <QDebug>

MainWindow::MainWindow(QWidget *parent) : QMainWindow(parent)
{
    setWindow();
    establishMenuBar();

    actions = new Actions(this);

    QVBoxLayout *primeLayout = new QVBoxLayout;
    primeLayout->addWidget(actions);

    if (auto cast_value = dynamic_cast<QStackedWidget*>(actions))
        setCentralWidget(cast_value);
    else
        exit(EXIT_FAILURE);

    makeConnections();

}

void MainWindow::setWindow()
{
    setWindowTitle(APPLICATION_NAME);
    setMinimumSize(WIN_WIDTH, WIN_HEIGHT);
}

void MainWindow::establishMenuBar()
{
    menuBar = new MenuBar();
    setMenuBar(menuBar);
}

void MainWindow::makeConnections()
{
    connect(menuBar->newLessons(), SIGNAL(triggered(bool)), this, SLOT(changeToNewLesson()));
    connect(menuBar->notification(), SIGNAL(triggered(bool)), this, SLOT(changeToNotification()));
    connect(menuBar->summary(), SIGNAL(triggered(bool)), this, SLOT(changeToSummary()));
    connect(menuBar->lessons(), SIGNAL(triggered(bool)), this, SLOT(changeToLessons()));
    connect(menuBar->students(), SIGNAL(triggered(bool)), this, SLOT(changeToStudents()));
    connect(menuBar->aboutQt(), SIGNAL(triggered(bool)), this, SLOT(aboutQt()));
    connect(menuBar->smtpSettings(), SIGNAL(triggered(bool)), this, SLOT(changeToSMTPSettings()));
    connect(actions->getLessons()->getAddButton(), SIGNAL(clicked(bool)), this, SLOT(changeToNewLesson()));
}

void MainWindow::changeToNewLesson()
{
    actions->setCurrentIndex(Actions::NEWLESSON);
}

void MainWindow::changeToNotification()
{
    actions->setCurrentIndex(Actions::NOTIFICATION);
}

void MainWindow::changeToSummary()
{
    actions->setCurrentIndex(Actions::SUMMARY);
}

void MainWindow::changeToLessons()
{
    actions->setCurrentIndex(Actions::LESSONS);
}

void MainWindow::changeToStudents()
{
    actions->setCurrentIndex(Actions::STUDENTS);
}

void MainWindow::changeToSMTPSettings()
{
    actions->setCurrentIndex(Actions::SMTPSETTINGS);
}

void MainWindow::aboutQt()
{
    QMessageBox::aboutQt(this);
}
