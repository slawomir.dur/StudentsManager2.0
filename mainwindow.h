#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include "actions.h"
#include "menubar.h"

#define APPLICATION_NAME "Students Database Manager 2.0"
#define WIN_WIDTH 980
#define WIN_HEIGHT 600

class MainWindow : public QMainWindow
{
    Q_OBJECT
public:
    explicit MainWindow(QWidget *parent = nullptr);
private:
    void setWindow();
    void establishMenuBar();
    void makeConnections();
signals:

public slots:
    void changeToNewLesson();
    void changeToNotification();
    void changeToSummary();
    void changeToLessons();
    void changeToStudents();
    void changeToSMTPSettings();
    void aboutQt();
private:
   MenuBar *menuBar;
   Actions *actions;
};

#endif // MAINWINDOW_H
