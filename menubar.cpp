#include "menubar.h"

MenuBar::MenuBar(QWidget *parent) : QMenuBar(parent)
{
    initializeMenus();
    initializeActions();
    setActionsToMenus();
    addMenus();
}

void MenuBar::initializeActions()
{
    newLessonsA = new QAction;
    studentsA = new QAction;
    lessonsA = new QAction;
    summaryA = new QAction;
    notificationA = new QAction;

    aboutQtA = new QAction;
    SMTPsettings = new QAction;

    newLessonsA->setText("New lesson");
    studentsA->setText("Students");
    lessonsA->setText("Lessons");
    summaryA->setText("Send Summary");
    notificationA->setText("Send notification");

    aboutQtA->setText("About Qt");
    SMTPsettings->setText(tr("SMTP Settings"));
}

void MenuBar::initializeMenus()
{
    fileM = new QMenu;
    helpM = new QMenu;

    fileM->setTitle("Action");
    helpM->setTitle("Help");
}

void MenuBar::setActionsToMenus()
{
    fileM->addAction(newLessonsA);
    fileM->addAction(studentsA);
    fileM->addAction(lessonsA);
    fileM->addAction(summaryA);
    fileM->addAction(notificationA);

    helpM->addAction(aboutQtA);
    helpM->addAction(SMTPsettings);
}

void MenuBar::addMenus()
{
    addMenu(fileM);
    addMenu(helpM);
}

