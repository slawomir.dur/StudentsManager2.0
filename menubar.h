#ifndef MENUBAR_H
#define MENUBAR_H

#include <QMenuBar>
#include <QMenu>

class MenuBar : public QMenuBar
{
public:
    MenuBar(QWidget *parent = nullptr);
    QAction* newLessons() {return newLessonsA;}
    QAction* students() {return studentsA;}
    QAction* lessons() {return lessonsA;}
    QAction* summary() {return summaryA;}
    QAction* notification() {return notificationA;}
    QAction* aboutQt() {return aboutQtA;}
    QAction* smtpSettings() {return SMTPsettings;}
protected:
    void initializeActions();
    void initializeMenus();
    void setActionsToMenus();
    void addMenus();
private:
    QMenu *fileM;
    QMenu *helpM;

    QAction *newLessonsA;
    QAction *studentsA;
    QAction *lessonsA;
    QAction *summaryA;
    QAction *notificationA;

    QAction *aboutQtA;
    QAction *SMTPsettings;
};

#endif // MENUBAR_H
