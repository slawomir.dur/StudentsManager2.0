#include "nameconverter.h"
#include <sstream>

QString convert(std::string nameWithSpace)
{
    std::stringstream stream;
    stream << nameWithSpace;
    std::string name;
    std::string surname;
    stream >> name >> surname;

    return QString(std::string(name+surname).c_str());
}

QString convert(QString nameWithSpace)
{
    return convert(nameWithSpace.toStdString());
}

std::pair<QString, QString> splitToSeparate(std::string nameWithSpace)
{
    std::stringstream stream;
    stream << nameWithSpace;
    std::string name;
    std::string surname;
    stream >> name >> surname;

    std::pair<QString, QString> qstringpair;
    qstringpair.first = QString(name.c_str());
    qstringpair.second = QString(surname.c_str());

    return qstringpair;
}
