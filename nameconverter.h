#ifndef NAMECONVERTER_H
#define NAMECONVERTER_H

#include <QString>
#include <QVariant>
#include <utility>


QString convert(std::string nameWithSpace);
QString convert(QString nameWithSpace);

std::pair<QString, QString> splitToSeparate(std::string nameWithSpace);

#endif // NAMECONVERTER_H
