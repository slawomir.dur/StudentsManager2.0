#include "newlesson.h"
#include <QVBoxLayout>
#include <QHBoxLayout>
#include <QLabel>
#include <QGroupBox>
#include <QDebug>
#include "database.h"
#include <QMessageBox>
#include "nameconverter.h"

NewLesson::NewLesson(QWidget *parent) : QWidget(parent)
{
    //qDebug() << "New lesson initialization";
    QVBoxLayout *primeLayout = new QVBoxLayout;
    QHBoxLayout *upperBox = new QHBoxLayout;
    QHBoxLayout *lowerBox = new QHBoxLayout;
    QHBoxLayout *groupBoxlayout = new QHBoxLayout;
    QVBoxLayout *leftUpperBox = new QVBoxLayout;
    QVBoxLayout *rightUpperBox = new QVBoxLayout;

    calendar = new QCalendarWidget;
    QLabel *studentLabel = new QLabel(tr("Student:"));
    //qDebug() << "ComboBoxModel init in New lesson";
    studentsBox = new ComboBoxModel;
    //qDebug() << "ComboBoxModel initialised in New Lesson";
    QLabel *subjectLabel = new QLabel(tr("Subject:"));
    subjectEdit = new QLineEdit;
    QLabel *homeLabel = new QLabel(tr("Homework:"));
    homeEdit = new QTextEdit;
    QGroupBox *notesGroupBox = new QGroupBox(tr("Notes"));
    notesEdit = new QTextEdit;
    saveButton = new QPushButton(tr("Save"));

    leftUpperBox->addWidget(studentLabel);
    leftUpperBox->addWidget(studentsBox);
    leftUpperBox->addWidget(subjectLabel);
    leftUpperBox->addWidget(subjectEdit);
    leftUpperBox->addWidget(homeLabel);
    leftUpperBox->addWidget(homeEdit);
    leftUpperBox->addWidget(saveButton);

    rightUpperBox->addWidget(calendar);

    groupBoxlayout->addWidget(notesEdit);
    notesGroupBox->setLayout(groupBoxlayout);

    lowerBox->addWidget(notesGroupBox);

    upperBox->addLayout(leftUpperBox);
    upperBox->addLayout(rightUpperBox);

    primeLayout->addLayout(upperBox);
    primeLayout->addLayout(lowerBox);

    setLayout(primeLayout);
    //qDebug() << "New lesson initialised";
    makeConnections();
}

void NewLesson::makeConnections()
{
    connect(saveButton, SIGNAL(clicked(bool)), this, SLOT(saveSlot()));
}

void NewLesson::saveSlot()
{
    //qDebug() << "saveSlot entered";

    if ((studentsBox->currentText() != QString("")) && (subjectEdit->text() != QString("")))
    {
        QVariant TableName(convert(studentsBox->currentText()));
        //qDebug() << "Passed table name: " << TableName.toString();
        QVariantList data;
        QString stringDate((QDate(calendar->selectedDate())).toString("dMMMyyyy"));
        data.append(subjectEdit->text());
        data.append(stringDate);
        data.append(homeEdit->toPlainText());
        data.append(notesEdit->toPlainText());

        Database db;
        db.connect();
        db.setMode(Database::LESSONS);
        //qDebug() << "db.insert()";
        if (db.insert(data, Database::LESSONS, &TableName))
        {
            //qDebug() << "db.insert() returned true";

            subjectEdit->setText("");
            homeEdit->setText("");
            notesEdit->setText("");

            QMessageBox msg(this);
            msg.setText(tr("Lesson added"));
            msg.setStandardButtons(QMessageBox::Ok);
            msg.exec();
            emit dataChanged();
        }
        else
        {
            QMessageBox msg(this);
            msg.setText(tr("Error with the database occurred."));
            msg.setStandardButtons(QMessageBox::Ok);
            msg.exec();
        }
    }
    else
    {
        QMessageBox msg(this);
        msg.setText(tr("Either no student chosen or no subject given!"));
        msg.setStandardButtons(QMessageBox::Ok);
        msg.exec();
    }


}
