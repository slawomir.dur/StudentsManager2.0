#ifndef NEWLESSON_H
#define NEWLESSON_H

#include <QWidget>
#include <QCalendarWidget>
#include <QLineEdit>
#include <QTextEdit>
#include <QPushButton>
#include "comboboxmodel.h"

class NewLesson : public QWidget
{
    Q_OBJECT
public:
    explicit NewLesson(QWidget *parent = nullptr);
    ComboBoxModel* getComboBox() {return studentsBox;}
private:
    void makeConnections();

private:
    QCalendarWidget *calendar;
    ComboBoxModel *studentsBox;
    QLineEdit *subjectEdit;
    QTextEdit *homeEdit;
    QTextEdit *notesEdit;
    QPushButton *saveButton;

signals:
    void dataChanged();

public slots:
    void saveSlot();
};

#endif // NEWLESSON_H
