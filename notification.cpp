#include "notification.h"
#include <QVBoxLayout>
#include <QLabel>
#include "mailmanager.h"
#include "database.h"

Notification::Notification(QWidget *parent) : QWidget(parent)
{
    QVBoxLayout *layout = new QVBoxLayout;

    QLabel *toLabel = new QLabel(tr("To:"));
    QLabel *subjectLabel = new QLabel(tr("Subject:"));
    QLabel *emailLabel = new QLabel(tr("Email:"));

    recipients = new ComboBoxModel;
    subjectEdit = new QLineEdit;
    mailEdit = new QTextEdit;
    sendButton = new QPushButton(tr("Send"));

    layout->addWidget(toLabel);
    layout->addWidget(recipients);
    layout->addWidget(subjectLabel);
    layout->addWidget(subjectEdit);
    layout->addWidget(emailLabel);
    layout->addWidget(mailEdit);
    layout->addWidget(sendButton);

    setLayout(layout);

    makeConnections();
}

void Notification::makeConnections()
{
    connect(sendButton, SIGNAL(clicked(bool)), this, SLOT(sendMail()));
}

void Notification::sendMail()
{
    QString nameandsurname = recipients->currentText();
    Database db;
    db.connect();
    QString mail = db.getStudentMail(nameandsurname);
    qDebug() << mail;
    QString text = mailEdit->toPlainText();
    MailManager manager;
   // manager.setUser("slawomir.dur@gmail.com");
   // manager.setPassword("061896sAurelka");
    manager.setSendingInfo(mail, nameandsurname, subjectEdit->text(), &text);
    if (manager.send())
        qDebug() << "sent";
    else qDebug() << "not sent";
}
