#ifndef NOTIFICATION_H
#define NOTIFICATION_H

#include <QWidget>
#include "comboboxmodel.h"
#include <QLineEdit>
#include <QTextEdit>
#include <QPushButton>

class Notification : public QWidget
{
    Q_OBJECT
public:
    explicit Notification(QWidget *parent = nullptr);
    ComboBoxModel* getComboBox() {return recipients;}
private:
    void makeConnections();
private:
    ComboBoxModel *recipients;
    QLineEdit *subjectEdit;
    QTextEdit *mailEdit;
    QPushButton *sendButton;

signals:

public slots:
    void sendMail();
};

#endif // NOTIFICATION_H
