#include "smtpsettings.h"
#include "mailmanager.h"
#include <QLabel>
#include <QHBoxLayout>
#include <QVBoxLayout>
#include <QMessageBox>

SMTPSettings::SMTPSettings(QWidget *parent) : QWidget(parent)
{
    QVBoxLayout *primeLayout = new QVBoxLayout;
    QHBoxLayout *userLayout = new QHBoxLayout;
    QHBoxLayout *userlabelLayout = new QHBoxLayout;
    QHBoxLayout *passwordLayout = new QHBoxLayout;
    QHBoxLayout *portLayout = new QHBoxLayout;

    QLabel *usernameLabel = new QLabel(tr("User:"));
    QLabel *userlabelLabel = new QLabel(tr("User label:"));
    QLabel *passwordLabel = new QLabel(tr("Password:"));
    QLabel *portLabel = new QLabel(tr("Port:"));

    usernameEdit = new QLineEdit;
    userlabelEdit = new QLineEdit;
    passwordEdit = new QLineEdit;
    portEdit = new QLineEdit;
    saveButton = new QPushButton(tr("Save settings"));

    userLayout->addWidget(usernameLabel);
    userLayout->addWidget(usernameEdit);

    userlabelLayout->addWidget(userlabelLabel);
    userlabelLayout->addWidget(userlabelEdit);

    passwordLayout->addWidget(passwordLabel);
    passwordLayout->addWidget(passwordEdit);

    portLayout->addWidget(portLabel);
    portLayout->addWidget(portEdit);

    primeLayout->addLayout(userLayout);
    primeLayout->addLayout(userlabelLayout);
    primeLayout->addLayout(passwordLayout);
    primeLayout->addLayout(portLayout);
    primeLayout->addWidget(saveButton);

    setLayout(primeLayout);

    makeConnections();
}

void SMTPSettings::makeConnections()
{
    connect(saveButton, SIGNAL(clicked(bool)), this, SLOT(save_slot()));
}

void SMTPSettings::save_slot()
{
    MailManager::User() = usernameEdit->text();
    MailManager::setUserLabel(userlabelEdit->text());
    MailManager::Password() = passwordEdit->text();
    MailManager::Port() = portEdit->text().toInt();

    QMessageBox msg(this);
    msg.setText("Settings set");
    msg.setStandardButtons(QMessageBox::Ok);
    msg.exec();
}
