#ifndef SMTPSETTINGS_H
#define SMTPSETTINGS_H

#include <QWidget>
#include <QPushButton>
#include <QLineEdit>

class SMTPSettings : public QWidget
{
    Q_OBJECT
public:
    explicit SMTPSettings(QWidget *parent = nullptr);
private:
    void makeConnections();
private:
    QPushButton *saveButton;
    QLineEdit *usernameEdit;
    QLineEdit *userlabelEdit;
    QLineEdit *passwordEdit;
    QLineEdit *portEdit;
    QLineEdit *smtpHostEdit;

signals:

public slots:
    void save_slot();
};

#endif // SMTPSETTINGS_H
