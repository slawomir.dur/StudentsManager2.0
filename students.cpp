#include "students.h"
#include "database.h"
#include <QVBoxLayout>
#include <QHBoxLayout>
#include <QMessageBox>
#include <QLabel>
#include <string>
#include <sstream>
#include <QDebug>
#include "nameconverter.h"

Students::Students(QWidget *parent) : QWidget(parent)
{
    addwindow = new AddWindow(this);
    removewindow = new RemoveWindow(this);
    QVBoxLayout *primeLayout = new QVBoxLayout;
    QHBoxLayout *buttonsLayout = new QHBoxLayout;

    QLabel *studentsLabel = new QLabel(tr("Students"));

    studentsTable = new StudentsTable;
    addButton = new QPushButton(tr("Add"));
    changeButton = new QPushButton(tr("Change"));
    removeButton = new QPushButton(tr("Remove"));
    removeAllButton = new QPushButton(tr("Remove All"));

    buttonsLayout->addWidget(addButton);
    buttonsLayout->addWidget(changeButton);
    buttonsLayout->addWidget(removeButton);
    buttonsLayout->addWidget(removeAllButton);

    primeLayout->addWidget(studentsLabel);
    primeLayout->addWidget(studentsTable);
    primeLayout->addLayout(buttonsLayout);

    setLayout(primeLayout);

    makeConnections();
}

void Students::makeConnections()
{
    connect(addButton, SIGNAL(clicked(bool)), this, SLOT(add_slot()));
    connect(removeButton, SIGNAL(clicked(bool)), this, SLOT(remove_slot()));
}

void Students::add_slot()
{
    addwindow->setModal(true);
    addwindow->exec();
}

void Students::remove_slot()
{
    removewindow->setModal(true);
    removewindow->exec();
}

AddWindow::AddWindow(QWidget *parent) : QDialog(parent)
{
    qDebug() << "addwindow comes to life";
    QHBoxLayout *nameBox = new QHBoxLayout;
    QHBoxLayout *surnameBox = new QHBoxLayout;
    QHBoxLayout *phoneBox = new QHBoxLayout;
    QHBoxLayout *mailBox = new QHBoxLayout;

    QHBoxLayout *buttons = new QHBoxLayout;

    QVBoxLayout *primeLayout = new QVBoxLayout;

    QLabel *title = new QLabel(tr("New student"));
    QLabel *nameLabel = new QLabel(tr("Name:"));
    QLabel *surnameLabel = new QLabel(tr("Surname:"));
    QLabel *phoneLabel = new QLabel(tr("Phone:"));
    QLabel *mailLabel = new QLabel(tr("Mail:"));
    nameEdit = new QLineEdit;
    surnameEdit = new QLineEdit;
    phoneEdit = new QLineEdit;
    mailEdit = new QLineEdit;
    add = new QPushButton(tr("Add"));
    cancel = new QPushButton(tr("Cancel"));

    nameBox->addWidget(nameLabel);
    nameBox->addWidget(nameEdit);

    surnameBox->addWidget(surnameLabel);
    surnameBox->addWidget(surnameEdit);

    phoneBox->addWidget(phoneLabel);
    phoneBox->addWidget(phoneEdit);

    mailBox->addWidget(mailLabel);
    mailBox->addWidget(mailEdit);

    buttons->addWidget(add);
    buttons->addWidget(cancel);

    primeLayout->addWidget(title);
    primeLayout->addLayout(nameBox);
    primeLayout->addLayout(surnameBox);
    primeLayout->addLayout(phoneBox);
    primeLayout->addLayout(mailBox);
    primeLayout->addLayout(buttons);

    setLayout(primeLayout);

    makeConnections();
}

void AddWindow::makeConnections()
{
    connect(add, SIGNAL(clicked(bool)), this, SLOT(add_slot()));
    connect(cancel, SIGNAL(clicked(bool)), this, SLOT(cancel_slot()));
}

void AddWindow::add_slot()
{
    if ((nameEdit->text() != QString("")) && (surnameEdit->text() != QString("")))
    {
        QVariantList list;
        list.append(nameEdit->text());
        list.append(surnameEdit->text());
        list.append(phoneEdit->text());
        list.append(mailEdit->text());
        Database db;
        db.connect();
        db.setMode(Database::STUDENTS);
        if (db.insert(list, Database::STUDENTS))
        {
            nameEdit->setText("");
            surnameEdit->setText("");
            phoneEdit->setText("");
            mailEdit->setText("");

            emit dataChanged();

            QMessageBox msg(this);
            msg.setText("Dodano klienta!");
            msg.setStandardButtons(QMessageBox::Ok);
            msg.exec();
        }
        else
        {
            QMessageBox msg(this);
            msg.setText(tr("Error with the database occurred."));
            msg.setStandardButtons(QMessageBox::Ok);
            msg.exec();
        }
    }
    else
    {
        QMessageBox msg(this);
        msg.setText(tr("Either name or surname is blank"));
        msg.setStandardButtons(QMessageBox::Ok);
        msg.exec();
    }

}

void AddWindow::cancel_slot()
{
    close();
}

RemoveWindow::RemoveWindow(QWidget *parent) : QDialog(parent)
{
    QVBoxLayout *nameLayout = new QVBoxLayout;
    QHBoxLayout *buttons = new QHBoxLayout;
    QVBoxLayout *primeLayout = new QVBoxLayout;

    QLabel *nameLabel = new QLabel(tr("Choose a student: "));

    studentsBox = new ComboBoxModel;

    delButton = new QPushButton(tr("Remove"));
    cancel = new QPushButton(tr("Cancel"));

    nameLayout->addWidget(nameLabel);
    nameLayout->addWidget(studentsBox);

    buttons->addWidget(delButton);
    buttons->addWidget(cancel);

    primeLayout->addLayout(nameLayout);
    primeLayout->addLayout(buttons);

    setLayout(primeLayout);

    makeConnections();
}

void RemoveWindow::makeConnections()
{
    connect(delButton, SIGNAL(clicked(bool)), this, SLOT(del_slot()));
    connect(cancel, SIGNAL(clicked(bool)), this, SLOT(cancel_slot()));
}

void RemoveWindow::del_slot()
{
    if (studentsBox->currentText() != QString(""))
    {
        QVariantList list;
        auto nameAndSurname = splitToSeparate(studentsBox->currentText().toStdString());

        list.append(nameAndSurname.first);
        list.append(nameAndSurname.second);

        Database db;
        db.connect();
        db.setMode(Database::STUDENTS);
        if (db.deleteRecord(list, Database::STUDENTS))
        {
            emit dataChanged();
        }
        else
        {
            QMessageBox msg(this);
            msg.setText(tr("Error with the database occurred."));
            msg.setStandardButtons(QMessageBox::Ok);
            msg.exec();
        }
    }
    else
    {
        QMessageBox msg(this);
        msg.setText(tr("No student chosen."));
        msg.setStandardButtons(QMessageBox::Ok);
        msg.exec();
    }

}

void RemoveWindow::cancel_slot()
{
    close();
}
