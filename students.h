#ifndef STUDENTS_H
#define STUDENTS_H

#include <QDialog>
#include <QWidget>
#include <QPushButton>
#include <QLineEdit>
#include "studentstable.h"
#include "comboboxmodel.h"
#include <QDebug>

class AddWindow;
class RemoveWindow;

class Students : public QWidget
{
    Q_OBJECT
public:
    explicit Students(QWidget *parent = nullptr);
    AddWindow* getAddWindow() {return addwindow;}
    RemoveWindow* getRemoveWindow() {return removewindow;}
    StudentsTable* getStudsTable() {return studentsTable;}
private:
    void makeConnections();
private:
    StudentsTable *studentsTable;
    QPushButton *addButton;
    QPushButton *changeButton;
    QPushButton *removeButton;
    QPushButton *removeAllButton;

    AddWindow *addwindow;
    RemoveWindow *removewindow;
signals:

public slots:
    void add_slot();
    void remove_slot();
};

class AddWindow : public QDialog
{
    Q_OBJECT
public:
    explicit AddWindow(QWidget *parent = nullptr);
private:
    void makeConnections();
private:
    QLineEdit *nameEdit;
    QLineEdit *surnameEdit;
    QLineEdit *phoneEdit;
    QLineEdit *mailEdit;
    QPushButton *add;
    QPushButton *cancel;
signals:
    void dataChanged();
public slots:
    void add_slot();
    void cancel_slot();
};

class RemoveWindow : public QDialog
{
    Q_OBJECT
public:
    explicit RemoveWindow(QWidget *parent = nullptr);
    ComboBoxModel* getComboBox() {return studentsBox;}
private:
    void makeConnections();
private:
    ComboBoxModel *studentsBox;
    QLineEdit *nameEdit;
    QLineEdit *surnameEdit;
    QPushButton *delButton;
    QPushButton *cancel;
signals:
    void dataChanged();
public slots:
    void del_slot();
    void cancel_slot();
};

#endif // STUDENTS_H
