#include "studentstable.h"
#include <QHeaderView>
#include <vector>
#include <QHBoxLayout>
#include "database.h"

StudentsTable::StudentsTable(QWidget *parent) : QWidget(parent)
{
    Database *db = new Database;
    db->connect();

    QStringList headers;
    headers.append(tr("Name"));
    headers.append(tr("Surname"));
    headers.append(tr("Phone"));
    headers.append(tr("Mail"));
    /*
    std::vector<QString> headers;
    headers.push_back(tr("Name"));
    headers.push_back(tr("Surname"));
    headers.push_back(tr("Mail"));
    headers.push_back(tr("Phone"));
    */
    tableModel = new QSqlTableModel(this);
    tableModel->setTable("students");

    for (auto i : headers)
    {
        static auto counter = 0;
        tableModel->setHeaderData(counter, Qt::Horizontal, i);
        ++counter;
    }


    tableView = new QTableView;
    tableView->setModel(tableModel);
    tableView->setSelectionBehavior(QAbstractItemView::SelectRows);
    tableView->setSelectionMode(QAbstractItemView::SingleSelection);
    tableView->resizeColumnsToContents();
    tableView->setEditTriggers(QAbstractItemView::NoEditTriggers);
    tableView->horizontalHeader()->setSectionResizeMode(QHeaderView::Stretch);

    tableModel->select();

    QHBoxLayout *layout = new QHBoxLayout;
    layout->addWidget(tableView);
    setLayout(layout);
}
