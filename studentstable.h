#ifndef STUDENTSTABLE_H
#define STUDENTSTABLE_H

#include <QWidget>
#include <QSqlTableModel>
#include <QTableView>

class StudentsTable : public QWidget
{
    Q_OBJECT
public:
    explicit StudentsTable(QWidget *parent = nullptr);
    QSqlTableModel* getTableModel() {return tableModel;}
    QTableView* getTable() {return tableView;}
private:
    QSqlTableModel *tableModel;
    QTableView *tableView;
signals:

public slots:
};

#endif // STUDENTSTABLE_H
