#include "summary.h"
#include <QLabel>
#include <QHBoxLayout>
#include <QVBoxLayout>

Summary::Summary(QWidget *parent) : QWidget(parent)
{
    QVBoxLayout *primeLayout = new QVBoxLayout;
    QHBoxLayout *sendLayout = new QHBoxLayout;
    QHBoxLayout *periodLayout = new QHBoxLayout;

    QLabel *sendToLabel = new QLabel(tr("Send to: "));
    QLabel *periodLabel = new QLabel(tr("Period: "));
    QLabel *additionalLabel = new QLabel(tr("Additional feedback: "));

    recipientsCombo = new ComboBoxModel;
    periodCombo = new QComboBox;
    additionalEdit = new QTextEdit;
    sendButton = new QPushButton(tr("Send"));

    sendLayout->addWidget(sendToLabel);
    sendLayout->addWidget(recipientsCombo);

    periodLayout->addWidget(periodLabel);
    periodLayout->addWidget(periodCombo);

    primeLayout->addLayout(sendLayout);
    primeLayout->addLayout(periodLayout);
    primeLayout->addWidget(additionalLabel);
    primeLayout->addWidget(additionalEdit);
    primeLayout->addWidget(sendButton);

    setLayout(primeLayout);
}
