#ifndef SUMMARY_H
#define SUMMARY_H

#include <QWidget>
#include "comboboxmodel.h"
#include <QComboBox>
#include <QTextEdit>
#include <QPushButton>

class Summary : public QWidget
{
    Q_OBJECT
public:
    explicit Summary(QWidget *parent = nullptr);
    ComboBoxModel* getComboBox() {return recipientsCombo;}
private:
    ComboBoxModel *recipientsCombo;
    QComboBox *periodCombo;
    QTextEdit *additionalEdit;
    QPushButton *sendButton;
};

#endif // SUMMARY_H
